# Running Against Time

Running Against Time is a 2D Platformer that was made in under 72 hours for [Ludum Dare 44](https://ldjam.com/events/ludum-dare/44/theme), where the theme was 'Your Life is Currency'. In this game you play against a constant counting down clock in levels with increasing challenge. Each time the clock runs out, you're sent back to the start to try again.

The game was made using Unity so the repository can be easily downloaded and run with any Unity Editor (I recommend Unity 2019.4.11f1 or above), however, if you'd rather play the game as an executable or in your browser, you can easily play the game on [my itch.io page](https://rahulio2d.itch.io/running-against-time)

## Controls
To control your character you only need to use the Arrow Keys or the WASD Keys, and you can use the Spacebar for jumping if preferred. All controls are explained in the intro level of the game.

## Credits
This game was solely made by me ([Rahul Sharma](https://www.rahulsharma.uk)), however, I did utilise some asset packs for the art of the game, these are linked below:
- [Game Collectables Pack](https://aamatniekss.itch.io/game-collectable-pack-rotating) - The gem in the game was obtained from here, but as it is a paid resource I have excluded from the repository.
- [Animated Pixel Adventurer](https://rvros.itch.io/animated-pixel-hero) - This asset pack contains the animations for the character.

No other assets were used during development, however, for the WebGL build of the game we utilised the [Better Minimal WebGL Template](https://seansleblanc.itch.io/better-minimal-webgl-template)
